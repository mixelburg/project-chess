#include"Pawn.h"
#include <iostream>

Pawn::Pawn(const Point& pos, const PieceColor col) : Piece(pos, PieceType::pawn, col) {
	firstMovment = true;
}

std::vector<Point> Pawn::movePoints(Piece *** board) {
	std::vector<Point> points;

	Point thisPos (this->getPosition());
	int buffer = 1;
	int pawnMovePoints[][2] = {
		{-1, 1},
		{-1, -1},
		{-1, 0},
		{-2, 0}
	};

	if (this->color == PieceColor::black) {
		buffer = -1;
	}
	if (board[thisPos.getXI() + pawnMovePoints[3][0] * buffer][thisPos.getYI()]->getColor() == PieceColor::transparent) {
		if (this->isFirstMov()) {
			try {
				points.push_back(Point(thisPos.getXI() + pawnMovePoints[3][0] * buffer, thisPos.getYI()));
				this->firstMovment = false;
			}
			catch (int& e) {};
		}
	}
	if (board[thisPos.getXI() + pawnMovePoints[2][0] * buffer][thisPos.getYI()]->getColor() == PieceColor::transparent) {
		try {
			points.push_back(Point(thisPos.getXI() + pawnMovePoints[2][0] * buffer, thisPos.getYI()));
			this->firstMovment = false;
		}
		catch (int& e) {};
	}
	for (int i = 0; i < 2; ++i) {
		if (this->color == PieceColor::black) {
			try {
				Point otherPos(thisPos.getXI() + pawnMovePoints[i][0] * buffer, thisPos.getYI() + pawnMovePoints[i][1]);
				if (board[otherPos.getXI()][otherPos.getYI()]->getColor() == PieceColor::white) {
					points.push_back(otherPos);
					this->firstMovment = false;
				}
			}
			catch (int e) {};
		}

		try {
			Point otherPos(thisPos.getXI() + pawnMovePoints[i][0] * buffer, thisPos.getYI() + pawnMovePoints[i][1]);
			if (this->color == PieceColor::white && board[otherPos.getXI()][otherPos.getYI()]->getColor() == PieceColor::black) {
				points.push_back(otherPos);
				this->firstMovment = false;
			}
		}
		catch (int e) {};
	}
	for (int i = 0; i < points.size(); i++)
	{
		if (isFriend(points[i], board)) points.erase(points.begin() + i);
	}
	return points;
}

bool Pawn::isFirstMov() const{
	return this->firstMovment;
}

std::vector<Point> Pawn::murderPoints(Piece*** board)
{
	std::vector<Point> points;
	Point thisPos(this->getPosition());
	int buffer = 1;
	int pawnMovePoints[][2] = {
		{-1, 1},
		{-1, -1} 
	};
	if (this->color == PieceColor::black) {
		buffer = -1;
	}

	for (int i = 0; i < 2; ++i) {
		if (this->color == PieceColor::black) {
			try {
				Point otherPos(thisPos.getXI() + pawnMovePoints[i][0] * buffer, thisPos.getYI() + pawnMovePoints[i][1]);
				if (board[otherPos.getXI()][otherPos.getYI()]->getColor() == PieceColor::white) {
					points.push_back(otherPos);
					this->firstMovment = false;
				}
			}
			catch (int e) {};
		}

		try {
			Point otherPos(thisPos.getXI() + pawnMovePoints[i][0] * buffer, thisPos.getYI() + pawnMovePoints[i][1]);
			if (this->color == PieceColor::white && board[otherPos.getXI()][otherPos.getYI()]->getColor() == PieceColor::black) {
				points.push_back(otherPos);
				this->firstMovment = false;
			}
		}
		catch (int e) {};
	}
	for (int i = 0; i < points.size(); i++)
	{
		if (isFriend(points[i], board)) points.erase(points.begin() + i);
	}
	return std::vector<Point>();
}
