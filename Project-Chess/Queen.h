#include"Piece.h"

class Queen :public Piece {
public:
	Queen(const Point& pos, const PieceColor col);
	/* returns a vector that contains all the posible points a player can move this piece to
	   board - the game board
	*/
	virtual std::vector<Point> movePoints(Piece*** board) override;
};
