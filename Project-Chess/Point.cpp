#include "Point.h"

Point::Point()
{
	setX(1);
	setY(1);
}

Point::Point(const std::string pos)
{
	setXY(pos);
}

Point::Point(const int x, const int y)
{
	setX(8 - x);
	setY(y + 1);
}

Point::Point(const Point& other)
{
	setX(other.x);
	setY(other.y);
}

Point::~Point()
{
}

bool Point::isValidPos(int pos)
{
	return pos >= 1 && pos <= 8;
}

int Point::letterToInt(char letter)
{
	return (int)letter - 96; 
}

int Point::charNumToInt(char num)
{
	return (int)num - 48; 
}

int Point::getX() const
{
	return this->x;
}

int Point::getY() const
{
	return this->y;
}

int Point::getXI() const
{
	return (8 - this->x);
}

int Point::getYI() const
{
	return this->y - 1;
}

void Point::setX(const int val)
{
	if (isValidPos(val)) this->x = val;
	else throw 5;
}

void Point::setY(const int val)
{
	if (isValidPos(val)) this->y = val;
	else throw 5;
}

void Point::setXY(const std::string pos)
{
	setX(charNumToInt(pos[1]));
	setY(letterToInt(pos[0]));
}

Point& Point::operator=(const Point& other)
{
	if (this != &other) {
		this->x = other.x;
		this->y = other.y;
	}
	return *this;
}

bool operator==(const Point& ths, const Point& other)
{
	return ths.x == other.x && ths.y == other.y;
}
