#include "Piece.h"
#include <iostream>

bool Piece::containsPoint(const std::vector<Point> points, const Point & point)
{
    return std::count(points.begin(), points.end(), point);
}

Piece::Piece(const Point& pos, const PieceType t, const PieceColor col) : position(pos), type(t), color(col)
{
}

Piece::~Piece()
{
}

int Piece::checkMove(Point& newPos, Piece *** board)
{
    if (this->position == newPos) return MoveCodes::error_same_pos;
    if (isFriend(newPos, board)) return MoveCodes::error_piece_at_dest;
    if (!(containsPoint(movePoints(board), newPos))) return MoveCodes::error_move;
   
    return MoveCodes::ok;
}

void Piece::move(Point& newPos, Piece *** board)
{
    board[this->position.getXI()][this->position.getYI()] = new Blank(this->position);
    this->position = newPos;
    board[newPos.getXI()][newPos.getYI()] = this;
}

bool Piece::isFriend(Point& pos, Piece *** board) const
{
    return this->color == board[pos.getXI()][pos.getYI()]->color;
}


PieceColor Piece::getColor() const
{
    return this->color;
}

PieceType Piece::getType() const
{
    return this->type;
}

void Piece::setPosition(const Point & newPosition)
{
    this->position = newPosition;
}

Point Piece::getPosition() const
{
    return Point(this->position);
}

Piece& Piece::operator=(const Piece& other)
{
    if (this != &other) {
        this->position = other.position;
        this->type = other.type;
        this->color = other.color;
    }
    return *this;
}

