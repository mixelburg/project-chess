#include"Queen.h"


Queen::Queen(const Point& pos, const PieceColor col) : Piece(pos, PieceType::queen, col) {
}

std::vector<Point> Queen::movePoints(Piece *** board) {
	std::vector<Point> points;
	int delta[][2]{
		{1,0},
		{-1,0},
		{0,1},
		{0,-1},
		{-1, 1},
		{-1, -1},
		{1, -1},
		{1,1}
	};
	for (int* i : delta) {
		Point curr(this->position);
		do //x - down, y - up
		{
			try {
				curr = Point(curr.getXI() + i[0], curr.getYI() + i[1]);
			}
			catch (int e) {
				break;
			}
			points.push_back(curr);
		} while (board[curr.getXI()][curr.getYI()]->getColor() == PieceColor::transparent);
	}
	return points;
}