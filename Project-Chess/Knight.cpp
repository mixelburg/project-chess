#include"Knight.h"


Knight::Knight(const Point& pos, const PieceColor col) : Piece(pos, PieceType::knight, col) {
}

std::vector<Point> Knight::movePoints(Piece *** board) {
	std::vector<Point> points;
	int remainderX = 0;
	int remainderY = 0;
	for (int i = 0; i < 8; ++i) {
		for (int j = 0; j < 8; j++)
		{
			if (board[i][j]->getColor() != this->color) {
				remainderX = this->getPosition().getXI() - board[i][j]->getPosition().getXI();
				remainderY = this->getPosition().getYI() - board[i][j]->getPosition().getYI();
				if ((remainderX == 2 || remainderX == -2) && (remainderY == 1 || remainderY == -1)) {
					points.push_back(Point(board[i][j]->getPosition().getXI(), board[i][j]->getPosition().getYI()));
				}
				else if ((remainderX == 1 || remainderX == -1) && (remainderY == 2 || remainderY == -2)) {
					points.push_back(Point(board[i][j]->getPosition().getXI(), board[i][j]->getPosition().getYI()));
				}
			}
		}
	}
	return points;
}