#pragma once

#include "Piece.h"

class BoardGame;

class BoardGame {
protected:
	int board_size;
	Piece*** board;
	std::string name;
	bool started = false;
public:
	BoardGame(const std::string n, const int b_size);
	virtual ~BoardGame();
};




