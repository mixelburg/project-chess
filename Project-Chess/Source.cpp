/*
This file servers as an example of how to use Pipe.h file.
It is recommended to use the following code in your project, 
in order to read and write information from and to the Backend
*/

#include "Pipe.h"
#include "stdafx.h"
#include <iostream>
#include <thread>

using std::cout;
using std::endl;
using std::string;


void main()
{
	srand(time_t(NULL));

	
	Pipe p;
	bool isConnect = p.connect();
	
	string ans;
	while (!isConnect)
	{
		cout << "cant connect to graphics" << endl;
		cout << "Do you try to connect again or exit? (0-try again, 1-exit)" << endl;
		std::cin >> ans;

		if (ans == "0")
		{
			cout << "trying connect again.." << endl;
			Sleep(5000);
			isConnect = p.connect();
		}
		else 
		{
			p.close();
			return;
		}
	}
	

	char msgToGraphics[1024];

	ChessGame* theGame = new ChessGame();
	
	theGame->printBoard();

	strcpy_s(msgToGraphics, theGame->start().c_str());
	cout << "[+] sending: " << msgToGraphics << std::endl << std::endl;	
	p.sendMessageToGraphics(msgToGraphics);   // send the board string

	// get message from graphics
	string msgFromGraphics = p.getMessageFromGraphics();

	while (msgFromGraphics != "quit")
	{
		cout << "[+] recieved: " << msgFromGraphics << std::endl;
		theGame->printBoard();
		int code = theGame->nextMove(msgFromGraphics);
		char code_c = code + 48;
		string msg = "";
		msg += code_c;
		strcpy_s(msgToGraphics, msg.c_str()); // msgToGraphics should contain the result of the operation
		cout << "[+] sending: " << msgToGraphics << std::endl << std::endl;

		theGame->printBoard();
		// return result to graphics		
		p.sendMessageToGraphics(msgToGraphics);   

		// get message from graphics
		msgFromGraphics = p.getMessageFromGraphics();
	}

	p.close();
	delete theGame;
}